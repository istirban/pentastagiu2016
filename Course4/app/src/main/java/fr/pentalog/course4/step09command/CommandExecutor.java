package fr.pentalog.course4.step09command;

import java.util.LinkedList;
import java.util.Queue;

public class CommandExecutor {
	private Queue<Command> commandQueue = new LinkedList<>();

	public void addCommand(Command command) {
		commandQueue.add(command);
	}

	public void executeAllCommands() {
		while (!commandQueue.isEmpty()) {
			final Command command = commandQueue.poll();
			command.execute();
		}
	}

}
