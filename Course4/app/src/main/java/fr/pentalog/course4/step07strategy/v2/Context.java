package fr.pentalog.course4.step07strategy.v2;

public class Context {
	private Operation operation;

	public Context(Operation operation) {
		this.operation = operation;
	}

	public int executeStrategy(int num1, int num2) {
		return this.operation.doOperation(num1, num2);
	}

}
