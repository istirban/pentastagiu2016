package fr.pentalog.course4.step01exceptions;

/**
 * Created by ionut on 3/20/2016.
 */
public class Exceptions4 {
    public static void main(String[] args){
        System.out.println("before exception");
        try {
            //int d = 100 / 0;
            if(true){
                throw new RuntimeException("Just an exception");
            }
        }catch (ArithmeticException e){
            System.out.println("caught exception");
        } finally {
            System.out.println("finally block");
        }
        System.out.println("after exception");

    }
}
