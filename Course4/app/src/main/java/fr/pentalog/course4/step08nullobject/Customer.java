package fr.pentalog.course4.step08nullobject;

public interface Customer {
	String getName();

	boolean isNull();
}
