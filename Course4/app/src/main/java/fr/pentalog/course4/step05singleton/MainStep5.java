package fr.pentalog.course4.step05singleton;

/**
 * Created by ionut on 3/20/2016.
 */
public class MainStep5 {
    public static void main(String[] args){
        DocumentsManager documentsManager = DocumentsManager.getInstance();
        documentsManager.displayDocuments();

        DocumentsManager.getInstance().displayDocuments();
    }
}
