package fr.pentalog.course4.step07strategy.v2;

public class OperationMultiply implements Operation {

	@Override
	public int doOperation(int num1, int num2) {
		return num1 * num2;

	}

}
