package fr.pentalog.course4.step07strategy;

import fr.pentalog.course4.step06factorymethod.*;

/**
 * Created by ionut on 3/20/2016.
 */
public class Nexus5Pencil implements Pencil {
    @Override
    public void draw(int startX, int startY, int endX, int endY) {
        System.out.println("[nexus 5] startX = [" + startX + "], startY = [" + startY + "], endX = [" + endX + "], endY = [" + endY + "]");
    }
}
