package fr.pentalog.course4.step09command;

public abstract class BaseCommand implements Command {
	protected final Light light;

	public BaseCommand(Light light) {
		this.light = light;
	}
}
