package fr.pentalog.course4.step07strategy;

/**
 * Created by ionut on 3/20/2016.
 */
public class PencilFactory {
    private static boolean isNexus5() {
        if (Math.random() % 2 == 0) {
            return true;
        }
        return false;
    }

    public static Pencil newPencil() {
        if (PencilFactory.isNexus5()) {
            return new Nexus5Pencil();
        }
        return new ConsolePencil();
    }
}
