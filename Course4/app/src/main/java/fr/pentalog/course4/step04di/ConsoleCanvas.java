package fr.pentalog.course4.step04di;

/**
 * Created by ionut on 3/20/2016.
 */
public class ConsoleCanvas implements Canvas {
    @Override
    public void drawLine(int startX, int startY, int endX, int endY) {
        System.out.println("startX = [" + startX + "], startY = [" + startY + "], endX = [" + endX + "], endY = [" + endY + "]");
    }
}
