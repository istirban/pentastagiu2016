package fr.pentalog.course4.step09command;

public class LightOnCommand extends BaseCommand {
	public LightOnCommand(Light light) {
		super(light);
	}

	@Override
	public void execute() {
		light.lightOn();
	}

}
