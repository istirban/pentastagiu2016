package fr.pentalog.course4.step07strategy.v2;

public interface Operation {
	int doOperation(int num1, int num2);
}
