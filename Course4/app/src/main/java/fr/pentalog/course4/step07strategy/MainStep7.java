package fr.pentalog.course4.step07strategy;

/**
 * Created by ionut on 3/20/2016.
 */
public class MainStep7 {
    public static void main(String[] args){
        Pencil pencil = PencilFactory.newPencil();

        Shape shape = new Shape();
        shape.draw(pencil);
    }
}
