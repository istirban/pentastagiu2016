package fr.pentalog.course4.step09command;

public interface Command {
	void execute();
}
