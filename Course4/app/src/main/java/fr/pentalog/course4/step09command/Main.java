package fr.pentalog.course4.step09command;

public class Main {

	public static void main(String[] args) {
		final CommandExecutor executor = new CommandExecutor();
		final Light light = new Light();
		final Command on = new LightOnCommand(light);
		final Command off = new LightOffCommand(light);

		executor.addCommand(on);
		executor.addCommand(on);
		executor.addCommand(off);
		executor.addCommand(off);
		executor.addCommand(on);
		executor.addCommand(off);
		executor.addCommand(on);
		executor.addCommand(off);
		executor.addCommand(on);
		executor.addCommand(off);
		executor.addCommand(off);
		executor.addCommand(off);
		executor.addCommand(on);
		executor.addCommand(off);
		executor.addCommand(off);
		executor.addCommand(on);

		executor.addCommand(off);

		executor.executeAllCommands();

	}

}
