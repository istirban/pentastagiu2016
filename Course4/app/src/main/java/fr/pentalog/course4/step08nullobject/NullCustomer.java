package fr.pentalog.course4.step08nullobject;

public class NullCustomer implements Customer {

	@Override
	public String getName() {
		return "Not available in the database";
	}

	@Override
	public boolean isNull() {
		return true;
	}

}
