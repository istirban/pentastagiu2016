package fr.pentalog.course4.step04di;

/**
 * Created by ionut on 3/20/2016.
 */
public interface Canvas {
    void drawLine(int startX, int startY, int endX, int endY);
}
