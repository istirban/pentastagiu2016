package fr.pentalog.course4.step01exceptions;

/**
 * Created by ionut on 3/20/2016.
 * https://www3.ntu.edu.sg/home/ehchua/programming/java/images/Exception_Classes.png
 */
public class Exceptions5 {
    public static void main(String[] args){
        System.out.println("before exception");
        try {
            int d = 100 / 0;
        }catch (ArithmeticException e){
            System.out.println("caught exception");
            throw e;//throw new MyFancyException();
        } finally {
            System.out.println("finally block");
        }
        System.out.println("after exception");

    }
}
