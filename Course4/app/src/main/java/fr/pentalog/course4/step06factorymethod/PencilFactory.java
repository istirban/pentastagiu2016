package fr.pentalog.course4.step06factorymethod;

/**
 * Created by ionut on 3/20/2016.
 */
public class PencilFactory {
    public static Pencil newPencil(){
        return new ConsolePencil();
    }
}
