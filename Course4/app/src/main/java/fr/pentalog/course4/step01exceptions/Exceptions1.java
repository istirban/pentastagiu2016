package fr.pentalog.course4.step01exceptions;

/**
 * Created by ionut on 3/20/2016.
 */
public class Exceptions1 {
    public static void main(String[] args){
        System.out.println("before exception");
        int d = 100/0;
        System.out.println("after exception");
        d++;
    }
}
