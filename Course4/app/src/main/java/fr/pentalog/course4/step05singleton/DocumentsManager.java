package fr.pentalog.course4.step05singleton;

/**
 * Created by ionut on 3/20/2016.
 */
public class DocumentsManager {
    private static DocumentsManager documentsManager;

    public static DocumentsManager getInstance(){
        if(documentsManager == null){
            documentsManager = new DocumentsManager();
        }

        return documentsManager;
    }
    public void displayDocuments(){
        System.out.println("display documents");
    }
}
