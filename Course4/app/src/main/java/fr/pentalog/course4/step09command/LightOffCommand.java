package fr.pentalog.course4.step09command;

public class LightOffCommand extends BaseCommand {
	public LightOffCommand(Light light) {
		super(light);
	}

	@Override
	public void execute() {
		light.lightOff();
	}

}
