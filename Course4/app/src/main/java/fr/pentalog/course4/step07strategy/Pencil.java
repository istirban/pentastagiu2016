package fr.pentalog.course4.step07strategy;

/**
 * Created by ionut on 3/20/2016.
 */
public interface Pencil {
    void draw(int startX, int startY, int endX, int endY);
}
