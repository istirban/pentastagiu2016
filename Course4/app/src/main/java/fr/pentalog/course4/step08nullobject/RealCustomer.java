package fr.pentalog.course4.step08nullobject;

public class RealCustomer implements Customer {
	private String name;

	public RealCustomer(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public boolean isNull() {
		return false;
	}

}
