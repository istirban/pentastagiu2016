package fr.pentalog.course4.step09command;

public class Light {
	private enum Mode {
		ON, OFF
	}

	private Mode mode = Mode.OFF;

	public void lightOn() {
		if (mode == Mode.ON) {
			System.out.println("Light is already on");
		} else {
			System.out.println("turning light on");
			mode = Mode.ON;
		}
	}

	public void lightOff() {
		if (mode == Mode.OFF) {
			System.out.println("Light is already off");
		} else {
			System.out.println("turning light off");
			mode = Mode.OFF;
		}
	}
}
