package fr.pentalog.course2.step01inheritance;

/**
 * Created by ionut on 3/6/2016.
 */
public class FootballPlayer extends Employee {
    private String position;
    private boolean suspended;
    private int redCards;
    private int yellowCards;

    public FootballPlayer() {
        super("George");
    }
}
