package fr.pentalog.course2.step06collections;

/**
 * Created by ionut on 3/6/2016.
 * sout
 * shape - rectangle
 */
public class MainStep6 {
    public static void main(String[] args){
        FootballPlayer[] players = new FootballPlayer[3];
        players[0] = new FootballPlayer();
        players[1] = new FootballPlayer();
        players[2] = new FootballPlayer();

        Employee[] employees = new Employee[3];
        employees[0] = new FootballPlayer();
        employees[1] = new Coach();
        employees[2] = new FootballPlayer();

        for(Employee employee : employees){
            employee.getFormattedDetails();
        }


    }

}
