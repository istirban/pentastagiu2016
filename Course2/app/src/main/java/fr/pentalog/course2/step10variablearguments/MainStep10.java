package fr.pentalog.course2.step10variablearguments;

/**
 * Created by ionut on 3/6/2016.
 */
public class MainStep10 {
    public static void main(String[] args){
        Calculator calculator = new Calculator();
        calculator.sum(1, 3, 4, 5, 6, 7, 8, 9);
    }
}
