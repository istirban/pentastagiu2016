package fr.pentalog.course2.step06collections;

/**
 * Created by ionut on 3/6/2016.
 */
public class Employee {
    private String name;
    private double salary;
    private String birthdate;

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getFormattedDetails() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", salary=" + salary +
                ", birthdate='" + birthdate + '\'' +
                '}';
    }
}
