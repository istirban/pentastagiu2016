package fr.pentalog.course2.step07instanceof;

/**
 * Created by ionut on 3/6/2016.
 * sout
 * shape - rectangle
 */
public class MainStep7 {
    public static void main(String[] args){

        Employee[] employees = new Employee[3];
        employees[0] = new FootballPlayer();
        employees[1] = new Coach();
        employees[2] = new FootballPlayer();

        for (int i = 0; i < employees.length; i++) {
            if(employees[i] instanceof Employee){
                //
            }else if(employees[i] instanceof FootballPlayer){
                //do something for footbal player
            } else if(employees[i] instanceof Coach){
                //
            }

        }


    }

}
