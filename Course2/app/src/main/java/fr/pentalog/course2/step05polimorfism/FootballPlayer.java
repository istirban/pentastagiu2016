package fr.pentalog.course2.step05polimorfism;

/**
 * Created by ionut on 3/6/2016.
 */
public class FootballPlayer extends Employee {
    private String position;
    private boolean suspended;
    private int redCards;
    private int yellowCards;
    //super can be used for data attributes and methods
    @Override
    public String getFormattedDetails() {
        return super.getFormattedDetails()+" <<|||>> "+"FootballPlayer{" +
                "position='" + position + '\'' +
                ", suspended=" + suspended +
                ", redCards=" + redCards +
                ", yellowCards=" + yellowCards +
                '}';
    }
}
