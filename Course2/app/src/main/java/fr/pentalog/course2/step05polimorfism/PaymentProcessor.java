package fr.pentalog.course2.step05polimorfism;

/**
 * Created by ionut on 3/6/2016.
 */
public class PaymentProcessor {
    private double currentBalance = 1000000;
    public void pay(Employee employee){
        //transfer money to employee account
        currentBalance -= employee.getSalary();
    }
}
