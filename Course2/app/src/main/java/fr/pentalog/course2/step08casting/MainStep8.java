package fr.pentalog.course2.step08casting;

/**
 * Created by ionut on 3/6/2016.
 * sout
 * shape - rectangle
 */
public class MainStep8 {
    public static void main(String[] args){

        Employee employee = new FootballPlayer();



       // FootballPlayer footballPlayer = (FootballPlayer)employees[0];

        //or

        if(employee instanceof FootballPlayer){
            FootballPlayer footballPlayer1 = (FootballPlayer)employee;
            footballPlayer1.anotherMethod();

        }


    }

}
