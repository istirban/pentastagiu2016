package fr.pentalog.course2.step09overloading;

/**
 * Created by ionut on 3/6/2016.
 * the name must be the same
 */
public class AndroidLogger {
    public void log(int i){
        System.out.println("i = " + i);
    };
    public void log(float f){
        System.out.println("f = " + f);
    };
    private void log(double d){
        System.out.println("d = " + d);
    };
    public void log(int i, double d){
        log(i);
        System.out.println("and");
        log(d);
    };
    public void log(String s){
        System.out.println("s = " + s);
    };

}
