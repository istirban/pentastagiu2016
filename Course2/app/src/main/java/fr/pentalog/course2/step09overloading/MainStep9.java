package fr.pentalog.course2.step09overloading;

/**
 * Created by ionut on 3/6/2016.
 */
public class MainStep9 {
    public static void main(String[] args){
        AndroidLogger logger = new AndroidLogger();
        logger.log(1.2f);
        logger.log(1);
        logger.log("Step 9");
        logger.log(2);

    }
}
