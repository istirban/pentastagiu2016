package fr.pentalog.course2.step12objectclass;

/**
 * Created by ionut on 3/6/2016.
 */
public class Employee{
    private String name;
    private double salary;
    private String birthdate;

    public Employee(String name, double salary, String birthdate) {
        this.name = name;
        this.salary = salary;
        this.birthdate = birthdate;
    }
    public Employee(String name, double salary) {
        this(name, salary, null);
    }

    public Employee() {

    }

    public String getName() {
        return name;
    }

    public double getSalary() {
        return salary;
    }

    public String getBirthdate() {
        return birthdate;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", salary=" + salary +
                ", birthdate='" + birthdate + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Employee employee = (Employee) o;

        if (Double.compare(employee.salary, salary) != 0) return false;
        if (name != null ? !name.equals(employee.name) : employee.name != null) return false;
        return !(birthdate != null ? !birthdate.equals(employee.birthdate) : employee.birthdate != null);

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = name != null ? name.hashCode() : 0;
        temp = Double.doubleToLongBits(salary);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (birthdate != null ? birthdate.hashCode() : 0);
        return result;
    }
}
