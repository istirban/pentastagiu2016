package fr.pentalog.course2.step03methodoverriding;

/**
 * Created by ionut on 3/6/2016.
 */
public class Employee {
    private String name;
    private double salary;
    private String birthdate;


    public String getFormattedDetails() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", salary=" + salary +
                ", birthdate='" + birthdate + '\'' +
                '}';
    }
}
