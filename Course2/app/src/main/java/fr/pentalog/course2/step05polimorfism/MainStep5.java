package fr.pentalog.course2.step05polimorfism;

/**
 * Created by ionut on 3/6/2016.
 * sout
 * shape - rectangle
 */
public class MainStep5 {
    public static void main(String[] args){
        FootballPlayer player = new FootballPlayer();
        player.setSalary(1000);

        PaymentProcessor paymentProcessor = new PaymentProcessor();
        paymentProcessor.pay(player);
    }

}
