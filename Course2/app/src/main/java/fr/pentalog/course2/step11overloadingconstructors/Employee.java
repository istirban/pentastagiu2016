package fr.pentalog.course2.step11overloadingconstructors;

/**
 * Created by ionut on 3/6/2016.
 */
public class Employee {
    private String name;
    private double salary;
    private String birthdate;

    public Employee(String name, double salary, String birthdate) {
        this.name = name;
        this.salary = salary;
        this.birthdate = birthdate;
    }

    public Employee(String name, double salary) {
        this(name, salary, null);

    }
    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getFormattedDetails() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", salary=" + salary +
                ", birthdate='" + birthdate + '\'' +
                '}';
    }
}
