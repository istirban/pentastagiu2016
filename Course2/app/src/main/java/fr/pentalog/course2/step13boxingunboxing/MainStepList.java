package fr.pentalog.course2.step13boxingunboxing;

import java.util.LinkedList;

/**
 * Created by ionut on 3/8/2016.
 */
public class MainStepList {
    private static void printList(LinkedList<String> list){
        System.out.println();
        for(String item:list){
            System.out.print(item + " ");
        }
    }
    public static void main(String[] args){
        LinkedList<String> list = new LinkedList<>();
        list.add("George");
        printList(list);
        list.add("Jack");
        printList(list);
        list.remove(0);
        printList(list);
        list.add("Kim");
        list.add("Scott");
        printList(list);


    }
}
