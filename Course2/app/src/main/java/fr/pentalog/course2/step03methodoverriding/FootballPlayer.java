package fr.pentalog.course2.step03methodoverriding;

/**
 * Created by ionut on 3/6/2016.
 */
public class FootballPlayer extends Employee {
    private String position;
    private boolean suspended;
    private int redCards;
    private int yellowCards;

    @Override
    public String getFormattedDetails() {
        return "FootballPlayer{" +
                "position='" + position + '\'' +
                ", suspended=" + suspended +
                ", redCards=" + redCards +
                ", yellowCards=" + yellowCards +
                '}';
    }
}
