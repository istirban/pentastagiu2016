package fr.pentalog.course2.step10variablearguments;

/**
 * Created by ionut on 3/6/2016.
 */
public class Calculator {
    public int sum(int i1, int i2){
        return i1+i2;
    }
    public int sum(int i1, int i2, int i3){
        return i1+i2+i3;
    }
    public int sum(int... numbers){
        int sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            sum += numbers[i];
        }
        return sum;
    }
}
