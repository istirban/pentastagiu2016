package fr.pentalog.course3.step07abstract;

/**
 * Created by ionut on 3/13/2016.
 */
public abstract class Gun {
    private void normalMethod(){

    }
    public abstract void shoot();
}
