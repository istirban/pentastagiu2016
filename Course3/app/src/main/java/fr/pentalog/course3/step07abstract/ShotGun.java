package fr.pentalog.course3.step07abstract;

/**
 * Created by ionut on 3/13/2016.
 */
public class ShotGun extends Gun {
    @Override
    public void shoot() {
        System.out.println("shooting with a gun");
    }
}
