package fr.pentalog.course3.step06staticimport;

import fr.pentalog.course3.step01homework.Node;

import static fr.pentalog.course3.step06staticimport.AssertionUtil.*;

/**
 * Created by ionut on 3/12/2016.
 */
public class MainStep6 {
    public static void main(String[] args){
        String s1 = "s1";
        String s2 = "s2";
        assertIfEquals(s1, s2);

    }
}
