package fr.pentalog.course3.step01homework;

import java.util.Arrays;

/**
 * Created by ionut on 3/12/2016.
 */
public class ArrayListV<T> {
    private static final int DEFAULT_NR_OF_ELEMENTS = 5;
    private Object[] items;
    private int size = 0;//nr de elemente adaugate in lista

    public ArrayListV(int size) {
        items = new Object[size];

    }

    public ArrayListV() {
        this(DEFAULT_NR_OF_ELEMENTS);
    }

    public void add(T item) {
        if (size == items.length) {
            items = Arrays.copyOf(items, items.length * 2);
        }
        items[size] = item;
        size++;
    }

    public void remove(int index) {
        if (index < 0 || index > size) {
            throw new IndexOutOfBoundsException("Invalid index: " + index);
        }
        System.arraycopy(items, index+1, items, index, size-index-1);

        size--;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(items.length * 50);
        sb.append("[ ");
        for (int i = 0; i < size; i++) {
            if (i != 0) {
                sb.append(',');
            }
            sb.append(items[i]);
            sb.append(' ');
        }

        sb.append("]");

        return sb.toString();
    }
}
