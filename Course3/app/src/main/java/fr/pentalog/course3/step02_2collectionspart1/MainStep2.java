package fr.pentalog.course3.step02_2collectionspart1;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * Created by ionut on 3/13/2016.
 * ArrayList, LinkedList
 */
public class MainStep2 {
    public static void main(String[] args){
        final String removeString = "string 2";
        ArrayList<String> list = new ArrayList<>(5);
        list.add("string 1");
        list.add(removeString);
        list.add("string 3");

        Iterator<String> it = list.iterator();

        while(it.hasNext()){
            String val = it.next();
            if(val.equals(removeString)){
                it.remove();
            }
        }

//        for (String s : list) {
//            if(s.equals(removeString)){
//                list.remove(s);
//            }
//        }


        //LinkedList<String> list2 = new LinkedList<>();
    }
}
