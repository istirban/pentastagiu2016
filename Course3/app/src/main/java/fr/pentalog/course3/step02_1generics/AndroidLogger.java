package fr.pentalog.course3.step02_1generics;

/**
 * Created by ionut on 3/12/2016.
 */
public class AndroidLogger<T> {
    public void log(T t){
        System.out.println(t);
    }
    public<Q> void log2(Q q){
        System.out.println(q);
    }

}
