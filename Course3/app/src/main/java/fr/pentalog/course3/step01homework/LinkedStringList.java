package fr.pentalog.course3.step01homework;

/**
 * Created by ionut on 3/12/2016.
 */
public class LinkedStringList {
    private Node first;
    private Node last;

    public void add(String newValue) {
        Node newNode = new Node(newValue);
        if (first == null) {
            first = last = newNode;
        } else {
            last.setNext(newNode);
            last = newNode;
        }
    }

    public void remove(int index) {
        if (first == null) {
            throw new IndexOutOfBoundsException("Invalid index: " + index + " the list is empty");
        }
        if (index == 0) {
            first = first.getNext();
            return;
        }

        int i = 0;
        Node previous = first;
        while (i < index - 1) {
            if (previous.getNext() == null) {
                throw new IndexOutOfBoundsException("Invalid index: " + index);
            }
            previous = previous.getNext();
            i++;
        }
        Node toBeRemoved = previous.getNext();
        previous.setNext(toBeRemoved.getNext());

        if (toBeRemoved == last) {
            last = previous;
        }
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("[ ");
        Node node = first;
        while (node != null) {
            if (node != first) {
                sb.append(',');
            }
            sb.append(node.getValue());
            sb.append(' ');
            node = node.getNext();
        }
        sb.append("]");

        return sb.toString();
    }
}
