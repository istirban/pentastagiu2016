package fr.pentalog.course3.step08interfaces;

/**
 * Created by ionut on 3/13/2016.
 */
public interface PersonalDefenceGun {
    void defendYourself();
}
