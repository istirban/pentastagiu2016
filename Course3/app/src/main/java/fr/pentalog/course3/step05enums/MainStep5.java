package fr.pentalog.course3.step05enums;

/**
 * Created by ionut on 3/12/2016.
 */
public class MainStep5 {
    public static void main(String[] args){
        Command command = new Command();
        command.handleMode(ModeConstants.MODE_ON);
        command.handleMode(45);

//        EnumCommand enumCommand = new EnumCommand();
//        enumCommand.handleMode(23);
        AdvancedMode mode = AdvancedMode.OFF;
        System.out.println(mode.id);
    }
}
