package fr.pentalog.course3.step03static;

/**
 * Created by ionut on 3/12/2016.
 */
public class Counter {
    private static int count;
    static{
        System.out.println("static initializer");
    }
    public Counter(){
        System.out.println("counter");
        count++;
    }

    public static int getCount(){
        return count;
    }
}
