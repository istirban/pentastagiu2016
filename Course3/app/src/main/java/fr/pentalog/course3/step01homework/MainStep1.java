package fr.pentalog.course3.step01homework;

/**
 * Created by ionut on 3/12/2016.
 */
public class MainStep1 {

    public static void main(String[] args){
        ArrayListV stringList = new ArrayListV();
        stringList.add("Test");
        for(int i=0;i<100;i++){
            stringList.add("Item "+i);
        }
        System.out.println(stringList);

        stringList.remove(0);

        System.out.println(stringList);


    }
}
