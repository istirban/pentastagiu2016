package fr.pentalog.course3.step05enums;

/**
 * Created by ionut on 3/12/2016.
 */
public class EnumCommand {
    public void handleMode(Mode mode) {
        if (mode == Mode.ON) {
            System.out.println("mode one");
        } else {
            System.out.println("mode off");
        }
    }
}
