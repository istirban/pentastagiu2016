package fr.pentalog.course3.step08interfaces;

import fr.pentalog.course3.step07abstract.*;

/**
 * Created by ionut on 3/13/2016.
 */
public class Rifle implements Gun, PersonalDefenceGun {
    @Override
    public void shoot() {
        System.out.println("Shooting with a rifle");
    }

    @Override
    public void defendYourself() {

    }
}
