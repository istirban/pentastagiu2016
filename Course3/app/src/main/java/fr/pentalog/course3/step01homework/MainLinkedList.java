package fr.pentalog.course3.step01homework;

/**
 * Created by ionut on 3/12/2016.
 */
public class MainLinkedList {
    public static void main(String[] args){
        LinkedStringList stringList = new LinkedStringList();
        stringList.add("Test");
        for(int i=0;i<100;i++){
            stringList.add("Item "+i);
        }
        System.out.println(stringList);

        stringList.remove(3);

        System.out.println(stringList);
    }
}
