package fr.pentalog.course3.step02_1generics;

import fr.pentalog.course3.step01homework.Node;

/**
 * Created by ionut on 3/12/2016.
 */
public class MainStep2 {
    public static void main(String[] args){
        AndroidLogger<String> logger = new AndroidLogger<>();
        Node node = new Node("test");
        logger.log2(node);

        AndroidLogger<Node> nodeLogger = new AndroidLogger<>();
        nodeLogger.log(node);
    }
}
