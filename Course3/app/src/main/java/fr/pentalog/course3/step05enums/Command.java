package fr.pentalog.course3.step05enums;

/**
 * Created by ionut on 3/12/2016.
 */
public class Command {
    public void handleMode(int mode){
        if(mode == ModeConstants.MODE_ON){
            System.out.println("mode one");
        }else{
            System.out.println("mode off");
        }
    }
}
